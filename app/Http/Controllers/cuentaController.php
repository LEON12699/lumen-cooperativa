<?php

namespace App\Http\Controllers;

use App\Cliente;
use App\Cuenta;

use Laravel\Lumen\Routing\Controller as BaseController;
use App\Http\helped\responseBuilder;
use Illuminate\Http\Request;

class cuentaController extends BaseController
{

    public function getCuentaByNumero(Request $request, $numero){
    
        if ($request->isJson()){    // revisar esto
            $cuenta = Cuenta::where('numero',$numero)->get();
            
            if (!$cuenta-> isEmpty()){
                $status = true;
                $info = "data is listened sucessful";
            }
            else{
            $status= false;
            $info = "data not found";    
            }
    
        return  responseBuilder::result($status,$info, $cuenta);
        }else{
        $status= false;
        $info = "sin acceso";
        return  responseBuilder::result($status,$info);

        }
    
    }

    public function getCuenta(Request $request, $cedula){
        print($cedula);
        if ($request->isJson()){    // revisar esto
            $cliente = Cliente::where('cedula',$cedula)->first();
            print($cliente->cedula);
            $cuenta = Cuenta::where('cliente_id',$cliente->cliente_id)->get();
            

            if (!$cuenta-> isEmpty()){
                $status = true;
                $info = "data is listened sucessful";
            }
            else{
            $status= false;
            $info = "data not found";    
            }
    
        return  responseBuilder::result($status,$info, $cuenta);
        }else{
        $status= false;
        $info = "sin acceso";
        return  responseBuilder::result($status,$info);

        }
    
    }
    
    public function create(Request $request, $cedula){
        $cliente = Cliente::where('cedula',$cedula)->get();

        
        if(!$cliente -> isEmpty()){
        $cuenta = new Cuenta();
    	$cuenta -> numero = $request -> numero;
        $cuenta -> estado = true;
    	$cuenta -> fechaApertura = $request -> fechaApertura;
    	$cuenta -> tipoCuenta = $request -> tipoCuenta;
    	$cuenta -> saldo = $request -> saldo;
    	$cuenta -> cliente_id = $cliente -> id;

    	$cuenta->save();
        }else{
            return responseBuilder::result("no existe el cliente para la cuenta",404);
        }

        return response() -> json($cuenta);
        
    }
# por terminar seleccionar que cuenta es la que se va a modificar 
    public function modificar(Request $request, $cedula){
       
        // revisar esto no por forma data sino por defecto xx unreloded
        #$input = $request->all();
        $input = $request->except('numero');
        $cliente = Cliente::where('cedula',$cedula)-> first();
        $cuenta = Cuenta::where([
        ['cliente_id', $cliente->cliente_id],
            ['numero', '=', "$request->numero"]])->update($input);
        $status= true;
        $info = "update is sucessfull";
        return  responseBuilder::result($status,$info,$cuenta);
        
    

    }
 }
