<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Hash;
use illuminate\Hashing\BcryptHasher;
use App\Http\helped\responseBuilder;

use App\User;

use Exception;

use Illuminate\Http\Request;

use Laravel\Lumen\Routing\Controller as BaseController;

class UserController extends BaseController
{
    public function login(Request $request){
        $password = $request-> password;
        $username = $request-> username;

        $user = User::where('username',$username)->first();
        error_log($this->django_password_verify($password,$user->password));
        error_log($user->password);
        if(!empty($user)){
            if($this->django_password_verify($password,$user->password)){
                $status = true;
                $info = 'User is correct';
            }else{
                $status = false;
                $info = 'Useria  incorrect';
            }
        }else{
            $status = false;
                $info = 'User  incorrect';
        }
        return responseBuilder::result($status, $info);
    }
    
    public function django_password_verify(string $password, string $djangoHash):bool {
        $pieces = explode('$',$djangoHash);
        if(count($pieces) !== 4){
            throw new Exception('ilegañ has format');
        }
        list($header, $iter,$salt,$hash) = $pieces;
        if(preg_match('#^pbkdf2_([a-z0-9A-Z]+)$#',$header,$m)){
            $algo = $m[1];
        }else{
            throw new Exception('BAD HEADER {%s}',$header);

        }
        if (!in_array($algo,hash_algos()) ){
            throw new Exception(sprintf('ilegal hash algoritm (%s)',$algo));
            
        }
        // hash_pbdk2 -> genera una derivacion de clave pbkdf2 de una contraseñaproporcionada
        // algo es el nombre del algoritmos hash seleccionad (sha256,'')
        //salt = es un valor para la derivacion , Este valor deberia ser generado aletoriamente
        $calc = hash_pbkdf2(
            $algo,
            $password,
            $salt,
            (int)$iter,
            32,
            true
        );
        return hash_equals($calc,base64_decode($hash));

        
    }

    public function logout(Request $request){
        
    }
}
