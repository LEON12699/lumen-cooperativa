<?php

namespace App\Http\Controllers;
use App\Cuenta;
use App\Http\helped\responseBuilder;
use App\transaccion;

use Illuminate\Http\Request;
use Laravel\Lumen\Routing\Controller as BaseController;

class transaccionController extends BaseController
{
    public function index(Request $request){
        if ($request->isJson()){ 
            $cuenta = Cuenta::where('numero',$request->numero)->get();
            $cuenta =$cuenta[0];
            $transaccion = new transaccion();
            print($cuenta);
            if ( $cuenta != null){   
                
                $transaccion->fechaTransaccion = $request->fechaTransaccion;
                $transaccion->tipoTransaccion = $request->tipoTransaccion;
                
                if($transaccion->tipoTransaccion =='deposito'){
                    $valor = $request->valor; 
                    $cuenta->saldo = $cuenta->saldo+$valor;
                    $transaccion->valor = $valor;
                    $cuenta->save();
                }
                else if ( $transaccion->tipoTransaccion =='retiro'){
                    $valor = $request->valor; 
                    $cuenta->saldo = $cuenta->saldo-$valor;
                    $transaccion->valor = $valor;
                    $cuenta->save();
                }
                else {
                    return responseBuilder::result(false,'no hay esa transaccion');
                }

                $transaccion->descripcion = $request->descripcion;
                $transaccion->responsable = $request->responsable;
                
                $transaccion->cuenta_id = $cuenta->cuenta_id;
                $transaccion->save();

                $status =true;
                $info = 'deposito trealizado';
                return responseBuilder::result($status,$info,$transaccion);       
            }
            else{
                $status =false;
                $info = 'no existe cuenta';
                return responseBuilder::result($status,$info);
            }
        }else {
            $status =false;
            $info = 'transaccion is not done';
            return responseBuilder::result($status,$info);
        }
    
    
}   
}
