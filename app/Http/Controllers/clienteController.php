<?php

namespace App\Http\Controllers;

use App\Cliente;
use App\Cuenta;

use Laravel\Lumen\Routing\Controller as BaseController;
use App\Http\helped\responseBuilder;
use Illuminate\Http\Request;

class clienteController extends BaseController
{
    public function index(Request $request){
        $clientes = Cliente::all();

        return response ()->json($clientes, 200);
    }   //

    public function getCliente(Request $request, $cedula){
    
        if ($request->isJson()){    // revisar esto
            $cliente = Cliente::where('cedula',$cedula)->get();

            if (!$cliente-> isEmpty()){
                $status = true;
                $info = "data is listened sucessful";
            }
            else{
            $status= false;
            $info = "data not found";    
            }
    
        return  responseBuilder::result($status,$info, $cliente);
        }else{
        $status= false;
        $info = "sin acceso";
        return  responseBuilder::result($status,$info);

        }
    
    }
    
    public function create(Request $request){
        $cliente = new Cliente();

        $cliente -> cedula =  $request->cedula;
    
        $cliente -> nombre = $request->nombre;
        $cliente -> apellido =$request->apellido;
        $cliente -> genero = $request->genero;
        $cliente -> estadoCivil = $request->estadoCivil;
        $cliente -> fechaNacimiento = $request->fechaNacimiento;
        $cliente -> correo = $request->correo;
        $cliente -> telefono= $request->telefono;
        $cliente -> celular = $request->celular;
        $cliente -> direccion =$request->direccion;

        $cliente -> save();

        $cuenta = new Cuenta();
        
    	$cuenta -> numero = $request -> numero;

        $cuenta -> estado = true;
    	$cuenta -> fechaApertura = $request -> fechaApertura;
    	$cuenta -> tipoCuenta = $request -> tipoCuenta;
    	$cuenta -> saldo = $request -> saldo;
    	$cuenta -> cliente_id = $cliente -> id;

    	$cuenta->save();

        return response() -> json($cliente);
        
    }

    public function modificar(Request $request, $cedula){
       
        // revisar esto
        $input = $request->all();
        print_r($input);
        print($request);
        Cliente::where('cedula',$cedula)->update($input);
        $cliente = Cliente::where('cedula',$cedula)-> first();
        $status= true;
        $info = "update is sucessfull";
        return  responseBuilder::result($status,$info,$cliente);
        
    

    }
 }
