<?php

namespace App\Http\Controllers;

use Laravel\Lumen\Http\Request;
use Laravel\Lumen\Routing\Controller as BaseController;

class PruebaController extends BaseController
{
    public function index(Request $request){
        return response ()->json("saludo:prueba");
    }   //
}
