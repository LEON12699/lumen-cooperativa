<?php

namespace App;


use Illuminate\Database\Eloquent\Model;
use Laravel\Lumen\Auth\Authorizable;

class transaccion extends Model 
{
    protected $table= 'cliente_transaccion';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'fechaTransaccion', 'valor','descripcion','responsable','cuenta_id' ];

    public $timestamps = false;
    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    /**protected $hidden = [
        'password',
    ];*/
}
