<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->get('/', function () use ($router) {
    return $router->app->version();
});

$router ->get('/hola','PruebaController@index');

$router ->group(['prefix'=>'cliente'], function($router){
    $router ->get('','clienteController@index');
    $router ->get('/all','clienteController@index');
    $router ->get('/get/{cedula}','clienteController@getCliente');
    $router ->put('/modificar/{cedula}','clienteController@modificar');
    $router ->post('',"clienteController@create");
    
});

$router ->group(['prefix'=>'cuenta'], function($router){

    $router ->get('/get/{cedula}','cuentaController@getCuenta');
    $router ->put('/modificar/{cedula}','cuentaController@modificar');
    $router ->post('/{cedula}',"cuentaController@create");
    
});

$router ->group(['prefix'=>'transaccion'], function($router){

    $router ->post('/deposito',"transaccionController@index");
    
});

$router ->group(['prefix'=>'usuarios'], function($router){
    $router ->post('/ingresar','UserController@login');
    
});
